ARG NETBOX_VERSION=v3.7.8
ARG NETBOX_DOCKER_VERSION=2.8.0

FROM netboxcommunity/netbox:${NETBOX_VERSION}-${NETBOX_DOCKER_VERSION}

RUN /opt/netbox/venv/bin/pip install \
      --index-url https://artifactory.esss.lu.se/artifactory/api/pypi/pypi-virtual/simple \
      --no-warn-script-location \
      proxmoxer==2.0.1 \
      django-environ==0.10.0 \
      netbox-inventory-plugin==1.3.2 \
      netbox_custom_form_plugin==1.2.5

COPY --chown=1000:1000 --chmod=755 jquery-3.7.1.min.js /opt/netbox/netbox/static/netbox_custom_form_plugin/jquery/
